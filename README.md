# `Wydarzysko - Let's meet`

## Opis

`Wydarzysko - Let's meet` - to innowacyjna aplikacja społecznościowa stworzona z myślą o ludziach, którzy chcą poznać nowych ludzi i spędzić czas w miłym towarzystwie. Aplikacja skupia się na grupowych wyjściach oraz zespołowych aktywnościach. Dzięki niej możesz łatwo znaleźć interesujące wydarzenia w Twojej okolicy i nawiązać kontakt z ich uczestnikami oraz organizatorami.

## Funkcjonalności

### Interaktywna Mapa

`Wyświetlanie Wydarzeń` - Mapa pokazująca pinezki związane z różnymi wydarzeniami w okolicy.

`Szczegóły Wydarzenia` - Po kliknięciu w pinezkę, użytkownik otrzymuje krótki opis wydarzenia oraz link do pełnego profilu wydarzenia.

`Filtrowanie Wydarzeń` - Możliwość filtrowania wydarzeń na mapie według kategorii, daty czy lokalizacji.

### Wiadomości

`Bezpośredni Czat` - Umożliwia komunikację między użytkownikami w czasie rzeczywistym.

`Powiadomienia` - Automatyczne powiadomienia o nowych wiadomościach, nawet gdy aplikacja jest zamknięta.

`Grupowe Konwersacje` - Możliwość tworzenia grupowych czatów dla uczestników wydarzeń.

### Inteligentne Propozycje

`Rekomendacje Wydarzeń` - Bazujące na wcześniejszych aktywnościach użytkownika i jego preferencjach.

`Uczący się System AI` - Im więcej korzystasz z aplikacji, tym lepiej dostosowuje się do Twoich preferencji.

`Szybkie Dołączanie` - Prosta opcja dołączania do polecanych wydarzeń z poziomu powiadomienia.

### Tryby Wyglądu

`Tryb Ciemny` - Zmniejsza obciążenie dla oczu w niskim świetle.

`Tryb Kontrastowy` - Zwiększa czytelność dla osób z trudnościami w odbiorze wizualnym.

### Bezpieczeństwo Danych

`Dostęp do Danych` - Użytkownik ma pełny dostęp do swoich danych w aplikacji.

`Usuwanie Konta` - Możliwość trwałego usunięcia konta wraz z wszystkimi powiązanymi danymi.

`Zgody RODO` - Proces rejestracji zawiera wymagane zgody zgodnie z regulacjami RODO.

### Organizator

`Kalendarz Wydarzeń` - Przeglądaj swoje wydarzenia w miesięcznym kalendarzu.

`Przypomnienia` - Ustawiaj indywidualne przypomnienia dla wydarzeń, które Cię interesują.

`Synchronizacja` - Możliwość synchronizacji z kalendarzem wbudowanym w telefon.

### Profil Użytkownika

`Edycja Profilu` - Dodawanie zdjęć, opisów czy ulubionych cytatów.

`Status Aktywności` - Pokazuje obecny status użytkownika.

`Dodawanie do Przyjaciół` - Możliwość dodawania innych użytkowników do listy przyjaciół.

### Tworzenie Wydarzenia

`Formularz Wydarzenia` - Wprowadzanie nazwy, daty, miejsca i opisu wydarzenia.

`Prywatność` - Opcja ustawiająca wydarzenie na publiczne lub prywatne.

`Zarządzanie Uczestnikami` - Możliwość dodawania i usuwania uczestników oraz wysyłania do nich zaproszeń.

## Modele

Podczas naszej 24-godzinnej pracy nie udało nam się zaimplementować wielu rozwiązań z naszego, natomaist udało nam się wymodelować wiele z nich. Oto wyżej wspomniane modele.

### Diagram Klas

![Diagram Klas](models/class-diagrams/class-diagram.png)

[Link do Modelu](models/class-diagrams/class-diagram.png)

### Diagramy Przepływu - Rekomendacje Systemu AI

![Diagramy Przepływu - Rekomendacje Systemu AI](models/sequence-diagrams/ai-recommendation-system-sequence-diagram.png)

[Link do Modelu](models/sequence-diagrams/ai-recommendation-system-sequence-diagram.png)

### Diagram Przepływu - Utworzenie Wydarzenia

![Diagramy Przepływu - Utworzenie Wydarzenia](models/sequence-diagrams/create-event-sequence-diagram.png)

[Link do Modelu](models/sequence-diagrams/create-event-sequence-diagram.png)

## Autorzy

- `Olaf Tkaczyk`
- `Tomasz Wnuk`
- `Krzysztof Fijoł`
- `Ignacy Kuchciński`
- `Maksymilian Dudziak`
- `Jakub Gadomski`
