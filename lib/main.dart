import 'package:my_app/home.dart';
import 'package:my_app/mapa.dart';
import 'package:my_app/registration-second.dart';
import 'package:my_app/registration.dart';
import 'package:my_app/profile_opinie.dart';
import 'package:my_app/profile_zdjecia.dart';
import 'package:my_app/navbar.dart';
import 'package:my_app/wydarzenie.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => AppState()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

enum Views {
  home,
  registration,
  registrationSecond,
  mapa,
  profileOpinie,
  profileZdjecia,
  wydarzenia,
}

enum Menu {
  house,
  loupe,
  positive,
  mail,
  whiteman,
}

class AppState extends ChangeNotifier {
  Views currentView = Views.registration;

  void switchToView(Views view) {
    this.currentView = view;
    notifyListeners();
  }

  Menu currentMenu = Menu.house;
  void switchToMenu(Menu menu) {
    this.currentMenu = menu;
  }

  getView() {
    switch (currentView) {
      case Views.registration:
        return Registration();
      case Views.registrationSecond:
        return RegistrationSecond();
      case Views.mapa:
        return Mapa();
      case Views.home:
        return Home();
      case Views.profileOpinie:
        return ProfileOpinie();
      case Views.profileZdjecia:
        return ProfileZdjecia();
      case Views.wydarzenia:
        return Wydarzenie();
      default:
        return Registration();
    }
  }

  bool shouldShowNavBar() {
    return ![Views.registration, Views.registrationSecond]
        .contains(currentView);
  }
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppState>(context);

    return Scaffold(
      bottomNavigationBar:
          appState.shouldShowNavBar() ? NavigationBarJB() : SizedBox(),
      body: Center(child: appState.getView()),
    );
  }
}
