import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:feather_icons/feather_icons.dart';

import 'main.dart';

class Wydarzenie extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppState>(context);
    return Stack(
      children: [
        Positioned(
          left: 0,
          top: 253,
          right: 0,
          bottom: 0,
          child: Container(
            width: 150,
            height: 150,
            decoration: BoxDecoration(
              color: Color(0xffffffff),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20, top: 20, right: 0, bottom: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Icon(
                                FeatherIcons.clock,
                                size: 24,
                                color: Color(0xff000000),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 5, top: 6, right: 0, bottom: 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Container(
                                width: 300,
                                height: 30,
                                margin: EdgeInsets.only(
                                    left: 10, top: 10, right: 10, bottom: 10),
                                padding: EdgeInsets.only(
                                    left: 10, top: 1, right: 1, bottom: 1),
                                decoration: BoxDecoration(
                                  color: Color(0xfff6f6f6),
                                  borderRadius: BorderRadius.circular(10),
                                  //boxShadow: [ BoxShadow(color: Color(0xff000000).withOpacity(FSize { size: 100 }), offset: Offset(0, 0), blurRadius: 1, spreadRadius: 8), ],
                                ),
                                child: Text(
                                  'Wpisz czas wydarzenia',
                                  style: GoogleFonts.getFont(
                                    'Poppins',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff888888),
                                    decoration: TextDecoration.none,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                  textDirection: TextDirection.ltr,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 20, top: 20, right: 0, bottom: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Icon(
                                    FeatherIcons.mapPin,
                                    size: 24,
                                    color: Color(0xff000000),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 5, top: 6, right: 0, bottom: 0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Container(
                                    width: 300,
                                    height: 30,
                                    margin: EdgeInsets.only(
                                        left: 10,
                                        top: 10,
                                        right: 10,
                                        bottom: 10),
                                    padding: EdgeInsets.only(
                                        left: 10, top: 1, right: 1, bottom: 1),
                                    decoration: BoxDecoration(
                                      color: Color(0xfff6f6f6),
                                      borderRadius: BorderRadius.circular(10),
                                      //boxShadow: [ BoxShadow(color: Color(0xff000000).withOpacity(FSize { size: 100 }), offset: Offset(0, 0), blurRadius: 1, spreadRadius: 8), ],
                                    ),
                                    child: Text(
                                      'Wyszukaj miejsce',
                                      style: GoogleFonts.getFont(
                                        'Poppins',
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff888888),
                                        decoration: TextDecoration.none,
                                        fontStyle: FontStyle.normal,
                                      ),
                                      textAlign: TextAlign.left,
                                      textDirection: TextDirection.ltr,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 20, top: 20, right: 0, bottom: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Icon(
                                        FeatherIcons.tag,
                                        size: 24,
                                        color: Color(0xff000000),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 5, top: 6, right: 0, bottom: 0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Container(
                                        width: 300,
                                        height: 30,
                                        margin: EdgeInsets.only(
                                            left: 10,
                                            top: 10,
                                            right: 10,
                                            bottom: 10),
                                        padding: EdgeInsets.only(
                                            left: 10,
                                            top: 1,
                                            right: 1,
                                            bottom: 1),
                                        decoration: BoxDecoration(
                                          color: Color(0xfff6f6f6),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          //boxShadow: [ BoxShadow(color: Color(0xff000000).withOpacity(FSize { size: 100 }), offset: Offset(0, 0), blurRadius: 1, spreadRadius: 8), ],
                                        ),
                                        child: Text(
                                          'Wpisz tagi',
                                          style: GoogleFonts.getFont(
                                            'Poppins',
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xff888888),
                                            decoration: TextDecoration.none,
                                            fontStyle: FontStyle.normal,
                                          ),
                                          textAlign: TextAlign.left,
                                          textDirection: TextDirection.ltr,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, top: 20, right: 0, bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Icon(
                            FeatherIcons.user,
                            size: 24,
                            color: Color(0xff000000),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(left: 5, top: 6, right: 0, bottom: 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            width: 300,
                            height: 30,
                            margin: EdgeInsets.only(
                                left: 10, top: 10, right: 10, bottom: 10),
                            padding: EdgeInsets.only(
                                left: 10, top: 1, right: 1, bottom: 1),
                            decoration: BoxDecoration(
                              color: Color(0xfff6f6f6),
                              borderRadius: BorderRadius.circular(10),
                              //boxShadow: [ BoxShadow(color: Color(0xff000000).withOpacity(FSize { size: 100 }), offset: Offset(0, 0), blurRadius: 1, spreadRadius: 8), ],
                            ),
                            child: Text(
                              'Nazwa wydarzenia',
                              style: GoogleFonts.getFont(
                                'Poppins',
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff888888),
                                decoration: TextDecoration.none,
                                fontStyle: FontStyle.normal,
                              ),
                              textAlign: TextAlign.left,
                              textDirection: TextDirection.ltr,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, top: 20, right: 0, bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Icon(
                            FeatherIcons.alertTriangle,
                            size: 24,
                            color: Color(0xff000000),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(left: 5, top: 6, right: 0, bottom: 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            width: 200,
                            height: 30,
                            margin: EdgeInsets.only(
                                left: 10, top: 10, right: 0, bottom: 10),
                            padding: EdgeInsets.only(
                                left: 10, top: 1, right: 1, bottom: 1),
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Text(
                              'Czy będzie alkohol?',
                              style: GoogleFonts.getFont(
                                'Poppins',
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff888888),
                                decoration: TextDecoration.none,
                                fontStyle: FontStyle.normal,
                              ),
                              textAlign: TextAlign.left,
                              textDirection: TextDirection.ltr,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 50, top: 10, right: 10, bottom: 10),
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 1, top: 1, right: 1, bottom: 1),
                            child: Switch(
                                value: true,
                                activeColor: Colors.blue,
                                onChanged: (bool value) {}),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: 350,
                          height: 100,
                          margin: EdgeInsets.only(
                              left: 1, top: 1, right: 1, bottom: 1),
                          padding: EdgeInsets.only(
                              left: 10, top: 1, right: 1, bottom: 1),
                          decoration: BoxDecoration(
                            color: Color(0xfff6f6f6),
                            borderRadius: BorderRadius.circular(10),
                            //boxShadow: [ BoxShadow(color: Color(0xff000000).withOpacity(FSize { size: 100 }), offset: Offset(0, 0), blurRadius: 1, spreadRadius: 8), ],
                          ),
                          child: Text(
                            'Napisz coś o wydarzeniu...',
                            style: GoogleFonts.getFont(
                              'Poppins',
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff444444),
                              decoration: TextDecoration.none,
                              fontStyle: FontStyle.normal,
                            ),
                            textAlign: TextAlign.left,
                            textDirection: TextDirection.ltr,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding:
                      EdgeInsets.only(left: 0, top: 10, right: 0, bottom: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 15, top: 0, right: 0, bottom: 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              width: 200,
                              height: 50,
                              margin: EdgeInsets.only(
                                  left: 20, top: 1, right: 10, bottom: 1),
                              padding: EdgeInsets.only(
                                  left: 2, top: 12, right: 1, bottom: 1),
                              decoration: BoxDecoration(
                                color: Color(0xffe45643),
                                borderRadius: BorderRadius.circular(100),
                                //boxShadow: [ BoxShadow(color: Color(0xff000000).withOpacity(FSize { size: 100 }), offset: Offset(0, 0), blurRadius: 1, spreadRadius: 8), ],
                              ),
                              child: Text(
                                'Stwórz wydarzenie',
                                style: GoogleFonts.getFont(
                                  'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xffffffff),
                                  decoration: TextDecoration.none,
                                  fontStyle: FontStyle.normal,
                                ),
                                textAlign: TextAlign.left,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            width: 100,
                            height: 50,
                            margin: EdgeInsets.only(
                                left: 20, top: 1, right: 10, bottom: 1),
                            padding: EdgeInsets.only(
                                left: 1, top: 1, right: 1, bottom: 1),
                            decoration: BoxDecoration(
                              color: Color(0xff3b88e0),
                              borderRadius: BorderRadius.circular(100),
                              //boxShadow: [ BoxShadow(color: Color(0xff000000).withOpacity(FSize { size: 100 }), offset: Offset(0, 0), blurRadius: 1, spreadRadius: 8), ],
                            ),
                            child: Icon(
                              FeatherIcons.unlock,
                              size: 24,
                              color: Color(0xffffffff),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          bottom: 590,
          child: Container(
            width: 150,
            height: 150,
            decoration: BoxDecoration(
              color: Color(0xff3b88e0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(left: 1, top: 1, right: 1, bottom: 1),
                  child: Padding(
                    padding:
                        EdgeInsets.only(left: 1, top: 1, right: 1, bottom: 1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 1, top: 1, right: 1, bottom: 1),
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 1, top: 1, right: 1, bottom: 1),
                            child: Text(
                              'Dodawanie wydarzenia',
                              style: GoogleFonts.getFont(
                                'Poppins',
                                fontSize: 24,
                                fontWeight: FontWeight.w300,
                                color: Color(0xffffffff),
                                decoration: TextDecoration.none,
                                fontStyle: FontStyle.normal,
                              ),
                              textAlign: TextAlign.left,
                              textDirection: TextDirection.ltr,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(left: 30, top: 30, right: 30, bottom: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Icon(
                        FeatherIcons.plusCircle,
                        size: 100,
                        color: Color(0xffffffff),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      'Dodaj zdjęcie z wydarzenia',
                      style: GoogleFonts.getFont(
                        'Poppins',
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        color: Color(0xfff1f1f1),
                        decoration: TextDecoration.none,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,
                      textDirection: TextDirection.ltr,
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
