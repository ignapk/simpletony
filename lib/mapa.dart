import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:url_launcher/url_launcher.dart';

class Mapa extends StatelessWidget {

  bool isButtonToAddMarkerClicked = false;

  List<Marker> markers = [
    Marker(
      width: 20.0,
      height: 20.0,
      rotate: true,
      point: LatLng(51.25, 22.56666),
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on,
          size: 30.0,
          color: Colors.red,
        ),
      ),
    ),
    Marker(
      width: 20.0,
      height: 20.0,
      rotate: true,
      point: LatLng(51.25, 22.56666),
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on,
          size: 30.0,
          color: Colors.red,
        ),
      ),
    ),
    Marker(
      width: 20.0,
      height: 20.0,
      rotate: true,
      point: LatLng(51.2599, 22.56666),
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on,
          size: 30.0,
          color: Colors.red,
        ),
      ),
    ),
    Marker(
      width: 20.0,
      height: 20.0,
      rotate: true,
      point: LatLng(51.10, 22.59666),
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on,
          size: 30.0,
          color: Colors.red,
        ),
      ),
    ),
    Marker(
      width: 20.0,
      height: 20.0,
      rotate: true,
      point: LatLng(51.2, 22.5),
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on,
          size: 30.0,
          color: Colors.red,
        ),
      ),
    ),
    Marker(
      width: 20.0,
      height: 20.0,
      rotate: true,
      point: LatLng(51.20, 19.94),
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on,
          size: 30.0,
          color: Colors.red,
        ),
      ),
    ),
  ];
  void _addEventWithMarker(TapPosition position, LatLng latlng) {
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // The title text which will be shown on the action bar
      ),
      body: Column(children: [
        Flexible(
          flex: 1,
          child: Text('ASD'),
        ),
        Flexible(
          flex: 2,
          child: FlutterMap(
            options: MapOptions(
              center: LatLng(51.25, 22.56666),
              zoom: 9.2,
              onTap: _addEventWithMarker,
            ),
            nonRotatedChildren: [
              RichAttributionWidget(
                attributions: [
                  TextSourceAttribution(
                    'OpenStreetMap contributors',
                    onTap: () => launchUrl(
                        Uri.parse('https://openstreetmap.org/copyright')),
                  ),
                ],
              ),
            ],
            children: [
              TileLayer(
                urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                userAgentPackageName: 'com.example.app',
              ),
              MarkerLayer(markers: markers),
            ],
          ),
        ),
        Flexible(
            flex: 1,
            child: TextButton(
              onPressed: () {
                isButtonToAddMarkerClicked = true;
              },
              child: Text('Add Event'), // Button text
              style: TextButton.styleFrom(
                primary: Colors.blue, // Text color
                padding: EdgeInsets.symmetric(
                    vertical: 64.0,
                    horizontal: 128.0), // Adjust the padding for size
              ),
            )),
      ]),
    );
  }
}
