import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'main.dart';

class RegistrationSecond extends StatelessWidget {
  @override
  build(BuildContext context) {
    final appState = Provider.of<AppState>(context);

    return Stack(
      children: [Positioned(
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        child: Container(
          width: 150,
          height: 150,


          decoration: BoxDecoration(
            color: Color(0xffffffff),


          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20, top: 30, right: 20, bottom: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      'Cofnij',
                      style: GoogleFonts.getFont(
                        'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff609ee6),
                        decoration: TextDecoration.none,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,

                      textDirection: TextDirection.ltr,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 3, top: 0, right: 80, bottom: 0),
                      child: Text(
                        'Rejestracja',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 32,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff000000),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.left,

                        textDirection: TextDirection.ltr,
                      ),
                    )
                    ,
                  ],
                ),
              )
              ,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 0, top: 2, right: 0, bottom: 0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),

                      ),
                      clipBehavior: Clip.hardEdge,
                      child: Image.network(
                        'https://fftefqqvfkkewuokofds.supabase.co/storage/v1/object/public/project-assets/afa3d54f-f5dc-4730-b64c-98f553f1930e/user-profile-icon-free-vector.jpg',
                        width: 150,
                        height: 150,
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                  ,
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      'Wybierz opisujące Cię tagi',
                      style: GoogleFonts.getFont(
                        'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                        decoration: TextDecoration.none,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,

                      textDirection: TextDirection.ltr,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 0, top: 30, right: 0, bottom: 0),
                      child: Text(
                        'Wpisz tutaj',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff888888),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.left,

                        textDirection: TextDirection.ltr,
                      ),
                    )
                    ,
                    Container(
                      width: 500,
                      height: 2,
                      margin: EdgeInsets.only(left: 0, top: 5, right: 0, bottom: 0),

                      decoration: BoxDecoration(
                        color: Color(0xff888888),


                      ),

                    ),
                  ],
                ),
              )
              ,
              Padding(
                padding: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 30),
                      child: Text(
                        'Wybierz swój typ osobowości',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff000000),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.left,

                        textDirection: TextDirection.ltr,
                      ),
                    )
                    ,
                    Text(
                      'Introwertyczny / Ekstrawertyczny / Nie chcę odp.',
                      style: GoogleFonts.getFont(
                        'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff000000),
                        decoration: TextDecoration.none,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,

                      textDirection: TextDirection.ltr,
                    ),
                    Container(
                      width: 500,
                      height: 2,
                      margin: EdgeInsets.only(left: 0, top: 5, right: 0, bottom: 0),

                      decoration: BoxDecoration(
                        color: Color(0xff888888),


                      ),

                    ),
                  ],
                ),
              )
              ,
              Padding(
                padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      'Co lubisz robić w wolnym czasie?',
                      style: GoogleFonts.getFont(
                        'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                        decoration: TextDecoration.none,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,

                      textDirection: TextDirection.ltr,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 0, top: 30, right: 0, bottom: 0),
                      child: Text(
                        'Wpisz tutaj',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff888888),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.left,

                        textDirection: TextDirection.ltr,
                      ),
                    )
                    ,
                    Container(
                      width: 500,
                      height: 2,
                      margin: EdgeInsets.only(left: 0, top: 5, right: 0, bottom: 0),

                      decoration: BoxDecoration(
                        color: Color(0xff888888),


                      ),

                    ),
                  ],
                ),
              )
              ,
              Padding(
                padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      'Jak wygląda dla Ciebie dobra impreza?',
                      style: GoogleFonts.getFont(
                        'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                        decoration: TextDecoration.none,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,

                      textDirection: TextDirection.ltr,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 0, top: 30, right: 0, bottom: 0),
                      child: Text(
                        'Wpisz tutaj',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff888888),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.left,

                        textDirection: TextDirection.ltr,
                      ),
                    )
                    ,
                    Container(
                      width: 500,
                      height: 2,
                      margin: EdgeInsets.only(left: 0, top: 5, right: 0, bottom: 0),

                      decoration: BoxDecoration(
                        color: Color(0xff888888),


                      ),

                    ),
                  ],
                ),
              )
              ,
              Padding(
                padding: EdgeInsets.only(left: 20, top: 0, right: 20, bottom: 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () {
                        appState.switchToView(Views.home);
                      },
                      child: Container(
                        width: 400,
                        height: 50,


                        decoration: BoxDecoration(
                          color: Color(0xff609ee6),
                          borderRadius: BorderRadius.circular(50),

                        ),
                        child: Padding(
                          padding: EdgeInsets.only(left: 0, top: 13, right: 0, bottom: 0),
                          child: Text(
                            'Dodaj',
                            style: GoogleFonts.getFont(
                              'Poppins',
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Color(0xffffffff),
                              decoration: TextDecoration.none,
                              fontStyle: FontStyle.normal,
                            ),
                            textAlign: TextAlign.center,

                            textDirection: TextDirection.ltr,
                          ),
                        )
                        ,
                      ),
                    )

                  ],
                ),
              )
              ,
              Padding(
                padding: EdgeInsets.only(left: 0, top: 30, right: 0, bottom: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      'Pomiń narazie',
                      style: GoogleFonts.getFont(
                        'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff609ee6),
                        decoration: TextDecoration.none,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,

                      textDirection: TextDirection.ltr,
                    ),
                  ],
                ),
              )
              ,
            ],
          ),
        ),
      )],
    );
  }
}