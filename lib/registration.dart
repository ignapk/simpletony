import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'main.dart';

class Registration extends StatelessWidget {
  @override
  build(BuildContext context) {
    final appState = Provider.of<AppState>(context);

    return Stack(
      children: [
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          child: Container(
            width: 150,
            height: 150,
            decoration: BoxDecoration(
              color: Color(0xffffffff),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding:
                  EdgeInsets.only(left: 20, top: 30, right: 20, bottom: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 0, top: 0, right: 20, bottom: 0),
                        child: Text(
                          'x',
                          style: GoogleFonts.getFont(
                            'Poppins',
                            fontSize: 24,
                            fontWeight: FontWeight.w300,
                            color: Color(0xff888888),
                            decoration: TextDecoration.none,
                            fontStyle: FontStyle.normal,
                          ),
                          textAlign: TextAlign.left,
                          textDirection: TextDirection.ltr,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 3, top: 0, right: 0, bottom: 0),
                        child: Text(
                          'Rejestracja',
                          style: GoogleFonts.getFont(
                            'Poppins',
                            fontSize: 32,
                            fontWeight: FontWeight.w600,
                            color: Color(0xff000000),
                            decoration: TextDecoration.none,
                            fontStyle: FontStyle.normal,
                          ),
                          textAlign: TextAlign.left,
                          textDirection: TextDirection.ltr,
                        ),
                      ),
                      Text(
                        'Login',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff609ee6),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.left,
                        textDirection: TextDirection.ltr,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.only(left: 20, top: 40, right: 20, bottom: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          color: Color(0xffe7e7e7),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        margin: EdgeInsets.only(bottom: 10),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 15, top: 0, right: 2, bottom: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(
                                'Imię',
                                style: GoogleFonts.getFont(
                                  'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff888888),
                                  decoration: TextDecoration.none,
                                  fontStyle: FontStyle.normal,
                                ),
                                textAlign: TextAlign.left,
                                textDirection: TextDirection.ltr,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          color: Color(0xffe7e7e7),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        margin: EdgeInsets.only(bottom: 10),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 15, top: 0, right: 2, bottom: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(
                                'Email',
                                style: GoogleFonts.getFont(
                                  'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff888888),
                                  decoration: TextDecoration.none,
                                  fontStyle: FontStyle.normal,
                                ),
                                textAlign: TextAlign.left,
                                textDirection: TextDirection.ltr,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          color: Color(0xffe7e7e7),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        margin: EdgeInsets.only(bottom: 10),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 15, top: 0, right: 15, bottom: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(
                                'Hasło',
                                style: GoogleFonts.getFont(
                                  'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff888888),
                                  decoration: TextDecoration.none,
                                  fontStyle: FontStyle.normal,
                                ),
                                textAlign: TextAlign.left,
                                textDirection: TextDirection.ltr,
                              ),
                              Text(
                                'Pokaż',
                                style: GoogleFonts.getFont(
                                  'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff609ee6),
                                  decoration: TextDecoration.none,
                                  fontStyle: FontStyle.normal,
                                ),
                                textAlign: TextAlign.left,
                                textDirection: TextDirection.ltr,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.only(left: 20, top: 30, right: 20, bottom: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        width: 25,
                        height: 25,
                        margin: EdgeInsets.only(
                            left: 0, top: 0, right: 10, bottom: 0),
                        decoration: BoxDecoration(
                          color: Color(0xffe7e7e7),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          'Wyrażam zgodę na przetwarzanie moich danych *',
                          style: GoogleFonts.getFont(
                            'Poppins',
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff000000),
                            decoration: TextDecoration.none,
                            fontStyle: FontStyle.normal,
                          ),
                          textAlign: TextAlign.left,
                          softWrap: true,
                          textDirection: TextDirection.ltr,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.only(left: 20, top: 0, right: 20, bottom: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      GestureDetector(
                        onTap: () {
                          // Obsługa zdarzenia naciśnięcia kontenera.
                          appState.switchToView(Views.registrationSecond);
                          // Dodaj tutaj kod, który ma zostać wykonany po kliknięciu.
                        },
                        child: Container(
                          width: 400,
                          height: 50,
                          margin: const EdgeInsets.only(left: 0, top: 80, right: 0, bottom: 0),
                          decoration: BoxDecoration(
                            color: const Color(0xff609ee6),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Padding(
                            padding: EdgeInsets.only(left: 0, top: 7, right: 0, bottom: 0),
                            child: Text(
                              'Zarejestruj się',
                              style: GoogleFonts.getFont(
                                'Poppins',
                                fontSize: 24,
                                fontWeight: FontWeight.w300,
                                color: Color(0xffffffff),
                                decoration: TextDecoration.none,
                                fontStyle: FontStyle.normal,
                              ),
                              textAlign: TextAlign.center,
                              textDirection: TextDirection.ltr,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
