import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:feather_icons/feather_icons.dart';

import 'main.dart';

class NavigationBarJB extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppState>(context);

    bool house = false, loupe = false, plus = false, mail = false, a = false;

    switch (appState.currentMenu) {
      case Menu.house:
        house = true;
        break;
      case Menu.loupe:
        loupe = true;
        break;
      case Menu.positive:
        plus = true;
        break;
      case Menu.mail:
        mail = true;
        break;
      case Menu.whiteman:
        a = true;
        break;
    }

    Color inactiveBG = Color(0xffe7e7e7);
    Color activeBG = Color(0xff609ee6);
    Color inactiveFG = Color(0xff000000);
    Color activeFG = Color(0xffffffff);

    return Container(
      height: 80,
      decoration: BoxDecoration(
        border: Border(top: BorderSide(color: Color(0xffBDC5CD))),
        color: Color(0xfffafafa),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () {
              appState.switchToMenu(Menu.house);
              appState.switchToView(Views.home);
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: house ? activeBG : inactiveBG,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(
                FeatherIcons.home,
                size: 24,
                color: house ? activeFG : inactiveFG,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              appState.switchToMenu(Menu.loupe);
              appState.switchToView(Views.mapa);
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: loupe ? activeBG : inactiveBG,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(
                FeatherIcons.search,
                size: 24,
                color: loupe ? activeFG : inactiveFG,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              appState.switchToMenu(Menu.positive);
              appState.switchToView(Views.wydarzenia);
            },
            child: Container(
              width: 45,
              height: 45,
              decoration: BoxDecoration(
                color: plus ? activeBG : inactiveFG,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(
                FeatherIcons.plus,
                size: 40,
                color: activeFG,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              appState.switchToMenu(Menu.positive);
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: mail ? activeBG : inactiveBG,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(
                FeatherIcons.mail,
                size: 24,
                color: mail ? activeFG : inactiveFG,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              appState.switchToMenu(Menu.whiteman);
              appState.switchToView(Views.profileZdjecia);
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: a ? activeBG : inactiveBG,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(
                FeatherIcons.user,
                size: 24,
                color: a ? activeFG : inactiveFG,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
