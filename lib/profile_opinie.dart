import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'main.dart';

class ProfileOpinie extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppState>(context);
    return Stack(
      children: [
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: double.maxFinite,
                height: 200,
                decoration: BoxDecoration(
                  color: Color(0xff609ee6),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, top: 20, right: 20, bottom: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            'Ustawienia',
                            style: GoogleFonts.getFont(
                              'Poppins',
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Color(0xffffffff),
                              decoration: TextDecoration.none,
                              fontStyle: FontStyle.normal,
                            ),
                            textAlign: TextAlign.left,
                            textDirection: TextDirection.ltr,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 0, top: 0, right: 16, bottom: 0),
                            child: Text(
                              'Profile',
                              style: GoogleFonts.getFont(
                                'Poppins',
                                fontSize: 32,
                                fontWeight: FontWeight.w600,
                                color: Color(0xffffffff),
                                decoration: TextDecoration.none,
                                fontStyle: FontStyle.normal,
                              ),
                              textAlign: TextAlign.left,
                              textDirection: TextDirection.ltr,
                            ),
                          ),
                          Text(
                            'Wyloguj',
                            style: GoogleFonts.getFont(
                              'Poppins',
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Color(0xffffffff),
                              decoration: TextDecoration.none,
                              fontStyle: FontStyle.normal,
                            ),
                            textAlign: TextAlign.left,
                            textDirection: TextDirection.ltr,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                //width: double.maxFinite,
                decoration: BoxDecoration(
                  color: Color(0xffffffff),
                ),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  //mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      width: double.maxFinite,
                      height: 60,
                      margin: EdgeInsets.only(
                          left: 50, top: 50, right: 50, bottom: 0),
                      decoration: BoxDecoration(),
                      child: Text(
                        'Wiktoria Nowak',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 32,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff000000),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.ltr,
                      ),
                    ),
                    Container(
                      width: double.maxFinite,
                      height: 60,
                      decoration: BoxDecoration(),
                      child: Text(
                        '"Życie to nie cel, lecz podróż"',
                        style: GoogleFonts.getFont(
                          'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff000000),
                          decoration: TextDecoration.none,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.ltr,
                      ),
                    ),
                    Container(
                      width: double.maxFinite,
                      height: 70,
                      margin: EdgeInsets.only(
                          left: 20, top: 0, right: 20, bottom: 0),
                      decoration: BoxDecoration(
                        color: Color(0xffe7e7e7),
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                              child: Container(
                            width: 150,
                            height: 150,
                            margin: EdgeInsets.only(
                                left: 3, top: 3, right: 3, bottom: 3),
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: Center(
                              child: Text(
                                'Opinie',
                                style: GoogleFonts.getFont(
                                  'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff609ee6),
                                  decoration: TextDecoration.none,
                                  fontStyle: FontStyle.normal,
                                ),
                                textAlign: TextAlign.center,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                          )),
                          Expanded(
                              child: Center(
                            child: GestureDetector(
                              onTap: () {
                                appState.switchToView(Views.profileZdjecia);
                              },
                              child: Text(
                                'Zdjęcia',
                                style: GoogleFonts.getFont(
                                  'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff888888),
                                  decoration: TextDecoration.none,
                                  fontStyle: FontStyle.normal,
                                ),
                                textAlign: TextAlign.left,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                          )),
                        ],
                      ),
                    ),
		    /*Row(
			children: [
				Container(
decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              clipBehavior: Clip.hardEdge,
                              child:
				Image.network(
                                'https://fftefqqvfkkewuokofds.supabase.co/storage/v1/object/public/project-assets/afa3d54f-f5dc-4730-b64c-98f553f1930e/profilowe.jpg',
                                width: 60,
                                height: 60,
                                fit: BoxFit.cover,
                              )),
				Column(
					children: [
					  Row(
						children: [
						  Text("Mateusz"),
						Text("Kiedy"),
						],
					  ),
					  Text("Dluga wiadomosc bardzo"),
					],
				),
			],
		    ),*/
		   ListTile(
			leading: CircleAvatar(
				backgroundImage: NetworkImage('https://fftefqqvfkkewuokofds.supabase.co/storage/v1/object/public/project-assets/afa3d54f-f5dc-4730-b64c-98f553f1930e/profilowe.jpg'),
			),
			title: Row(
				mainAxisAlignment: MainAxisAlignment.spaceBetween,
				children: [
					Text(
						"Tomasz",
						style: GoogleFonts.getFont(
                  'Poppins',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff000000),
                  decoration: TextDecoration.none,
                  fontStyle: FontStyle.normal,
                ),
					),
					SizedBox(width:16.0),
					Text(
						"2 tyg temu",
				//		style: 
					),
				],
			),
			subtitle: Text(
				"Spotkaliśmy się z Wiktorią na wspólne imprezowanie w Warszawie. Jest bardzo towarzyska i łatwo nawiązuje kontakt z ludźmi.",
style: GoogleFonts.getFont(
                  'Poppins',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff000000),
                  decoration: TextDecoration.none,
                  fontStyle: FontStyle.normal,
                ),

			),
		   ),
ListTile(
			leading: CircleAvatar(
				backgroundImage: NetworkImage('https://fftefqqvfkkewuokofds.supabase.co/storage/v1/object/public/project-assets/afa3d54f-f5dc-4730-b64c-98f553f1930e/profilowe.jpg'),
			),
			title: Row(
				mainAxisAlignment: MainAxisAlignment.spaceBetween,
				children: [
					Text(
						"Weronika",
						style: GoogleFonts.getFont(
                  'Poppins',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff000000),
                  decoration: TextDecoration.none,
                  fontStyle: FontStyle.normal,
                ),
					),
					SizedBox(width:16.0),
					Text(
						"3 tyg temu",
				//		style: 
					),
				],
			),
			subtitle: Text(
				"Miałam przyjemność spędzić dzień ze Sławkiem zwiedzając Kraków. Jest niesamowicie zorganizowany, zna wiele ciekawych miejsc, których nie znajdziecie w przewodnikach, a do tego ma świetne poczucie humoru.",
style: GoogleFonts.getFont(
                  'Poppins',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff000000),
                  decoration: TextDecoration.none,
                  fontStyle: FontStyle.normal,
                ),

			),
		   ),
ListTile(
			leading: CircleAvatar(
				backgroundImage: NetworkImage('https://fftefqqvfkkewuokofds.supabase.co/storage/v1/object/public/project-assets/afa3d54f-f5dc-4730-b64c-98f553f1930e/profilowe.jpg'),
			),
			title: Row(
				mainAxisAlignment: MainAxisAlignment.spaceBetween,
				children: [
					Text(
						"Tomasz",
						style: GoogleFonts.getFont(
                  'Poppins',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff000000),
                  decoration: TextDecoration.none,
                  fontStyle: FontStyle.normal,
                ),
					),
					SizedBox(width:16.0),
					Text(
						"4 miesiące temu",
				//		style: 
					),
				],
			),
			subtitle: Text(
				"Piotr ma dużo wiedzy o historii Gdańska i był świetnym przewodnikiem. Niestety, często przerywał opowieść by rozmawiać przez telefon, co było trochę denerwujące. Niemniej jednak, dowiedziałam się dzięki niemu wielu interesujących rzeczy.",
style: GoogleFonts.getFont(
                  'Poppins',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff000000),
                  decoration: TextDecoration.none,
                  fontStyle: FontStyle.normal,
                ),

			),
		   ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 120,
          top: 85,
          right: 120,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
            ),
            clipBehavior: Clip.hardEdge,
            child: Image.network(
              'https://www.thevectorimpact.com/wp-content/uploads/2020/05/LinkedIn-profile-examples-mira-anamae-300x269.png',
              width: 150,
              height: 150,
            ),
          ),
        ),
      ],
    );
  }
}
